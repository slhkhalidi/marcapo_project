import "./App.css";
import Login from "./Components/Login/Login";
import Register from "./Components/Register/Register";
import Addresses from "./Components/Addresses/Addresses";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Address from "./Components/Addresses/Address";
function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/login" element={<Login />} />
          <Route path="/registrierung" element={<Register />} />
          <Route path="/home" element={<Addresses />} />
          <Route path="/address" element={<Address />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

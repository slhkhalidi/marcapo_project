import { makeAutoObservable } from "mobx";

interface myAddress {
  id: string;
  vorname: string;
  nachname: string;
  adresse: string;
  telefonNumber: string;
  foto: string;
  email: string;
  geburtstag: string;
}

class Store {
  jwt: string = "";

  address: myAddress = {
    id: "",
    vorname: "",
    nachname: "",
    adresse: "",
    telefonNumber: "",
    foto: "",
    email: "",
    geburtstag: "",
  };

  constructor() {
    makeAutoObservable(this);
  }

  setJwt(myToken: string) {
    this.jwt = myToken;
  }

  setAddress(newAddress: myAddress) {
    this.address = newAddress;
  }

  setAddressfirstname(firstname: string) {
    this.address.vorname = firstname;
  }
  setAddresslastname(lastname: string) {
    this.address.nachname = lastname;
  }
  setAddressAddresse(adresse: string) {
    this.address.adresse = adresse;
  }
  setAddressEmail(email: string) {
    this.address.email = email;
  }
  setAddressPhone(telefonNumber: string) {
    this.address.telefonNumber = telefonNumber;
  }
  setAddressBirthday(geburtstag: string) {
    this.address.geburtstag = geburtstag;
  }
}

const store = new Store();
export default store;

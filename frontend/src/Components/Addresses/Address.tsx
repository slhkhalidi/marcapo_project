import { useEffect, useState } from "react";
import logo from "../Images/logo.png";
import { Link, useNavigate } from "react-router-dom";
import "./Address.css";
import store from "../MobX-Store/store";
import { observer } from "mobx-react";
import { ToastContainer, toast } from "react-toastify";

function Address() {
  const navigate = useNavigate();
  // Id of the Address that am looking for //
  // const id = localStorage.getItem("searcherId");
  //const id = store.idToEdit;

  const logout = (): void => {
    localStorage.setItem("jwt", "");
    navigate("/");
  };

  /**
   * Get the Address by ID token
   */

  async function getMyAddress(): Promise<void> {
    const token = localStorage.getItem("jwt");
    const id = localStorage.getItem("idToSearch");
    const request = await fetch(
      `http://localhost:8081/api/v1/adressBuch/${id}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((data) => store.setAddress({ ...data }))
      .catch((err) => console.log(err));
  }
  useEffect(() => {
    getMyAddress();
  }, []);

  const validateChanges = () => {
    toast.success("Änderungen erfolgreich durchgeführt", {
      position: "bottom-center",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "dark",
    });
    getMyAddress();
  };

  /**
   * Send and update the new Object
   */
  async function update(): Promise<void> {
    const token = localStorage.getItem("jwt");
    let status: string = "";
    const request = await fetch(
      `http://localhost:8081/api/v1/adressBuch/update/${store.address.id}`,
      {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          id: store.address.id,
          vorname: store.address.vorname,
          nachname: store.address.nachname,
          adresse: store.address.adresse,
          telefonNumber: store.address.telefonNumber,
          foto: "",
          email: store.address.email,
          geburtstag: store.address.geburtstag,
        }),
      }
    )
      .then((response) => response.json())
      .then((data) =>
        data.status === "success"
          ? alert("Änderungen erfolgreich genommen !!")
          : alert("Bitte versuchen Sie es nochmal !!")
      )
      .catch((err) => console.log(err));
  }

  return (
    <div>
      <div className="home-navbar">
        <div>
          <img src={logo} className="logo" />
        </div>
        <div>
          <button className="logout" onClick={() => logout()}>
            Logout
          </button>
        </div>
      </div>
      <hr className="ligne" />

      <div className="cardHeader">
        <div className="cards">
          <form className="form">
            <div className="mb-3">
              <label className="font-weight-bold form-label">Vorname</label>
              <input
                onChange={(e) => store.setAddressfirstname(e.target.value)}
                defaultValue={store.address.vorname}
                type="email"
                className="myInput form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>
            <div className="mb-3">
              <label className="font-weight-bold form-label">Nachname</label>
              <input
                onChange={(e) => store.setAddresslastname(e.target.value)}
                defaultValue={store.address.nachname}
                type="email"
                className="myInput form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>
            <div className="mb-3">
              <label className=" font-weight-bold form-label">Email</label>
              <input
                onChange={(e) => store.setAddressEmail(e.target.value)}
                defaultValue={store.address.email}
                type="email"
                className="myInput form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>
            <div className="mb-3">
              <label className="font-weight-bold form-label">Adresse</label>
              <input
                onChange={(e) => store.setAddressAddresse(e.target.value)}
                defaultValue={store.address.adresse}
                type="email"
                className="myInput form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>
            <div className="mb-3">
              <label className=" font-weight-bold form-label">
                Telefonnummer
              </label>
              <input
                onChange={(e) => store.setAddressPhone(e.target.value)}
                defaultValue={store.address.telefonNumber}
                type="email"
                className="myInput form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>
            <div className="mb-3">
              <label className="form-label font-weight-bold">Geburtstag</label>
              <input
                onChange={(e) => store.setAddressBirthday(e.target.value)}
                defaultValue={store.address.geburtstag}
                type="email"
                className="myInput form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>
          </form>
          <div>
            <button
              onClick={() => navigate("/home")}
              className="btn-primary myBtns"
            >
              Zurück
            </button>
            <button className="btn-success myBtns" onClick={() => update()}>
              Save
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default observer(Address);

import { useState, useEffect } from "react";
import store from "../MobX-Store/store";
import { observer } from "mobx-react";
import Login from "../Login/Login";
import { Link, useNavigate } from "react-router-dom";
import "./Address.css";
import logo from "../Images/logo.png";
import {
  BsFillPencilFill,
  BsFillTrashFill,
  BsPlusCircleFill,
} from "react-icons/bs";

interface myAddress {
  id: string;
  vorname: string;
  nachname: string;
  adresse: string;
  telefonNumber: string;
  foto: string;
  email: string;
  geburtstag: string;
}

function Adresses() {
  const [addresses, setAdresses] = useState<myAddress[]>([]);
  const navigate = useNavigate();
  const [id, setId] = useState<string>("");
  const [firstname, setFirstname] = useState<string>("");
  const [lastname, setLastname] = useState<string>("");
  const [address, setaddress] = useState<string>("");
  const [telefonnumber, setTelefonnumber] = useState<string>("");
  const [email, setMail] = useState<string>("");
  const [birthsday, setBirthsday] = useState<string>("");
  const [image, setImage] = useState<string>("");
  const [idToken, setIdToken] = useState<string>("");

  /**
   * Logout
   */
  const logout = (): void => {
    localStorage.setItem("jwt", "");
    navigate("/");
  };

  /**
   * Get All Data
   */
  function getAllAddress(): void {
    fetch("http://localhost:8081/api/v1/adressBuch/all")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setAdresses(data);
      });
  }
  useEffect(() => {
    getAllAddress();
  }, []);

  /**
   * Delete
   */
  async function removeOne(index: string): Promise<void> {
    const token = localStorage.getItem("jwt");
    const request = await fetch(
      `http://localhost:8081/api/v1/adressBuch/delete/${index}`,
      {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-type": "application/json",
        },
      }
    );
    const response = await request.text();
    console.log();
    if (response === "OK") {
      getAllAddress();
    } else {
      alert("Fehler, Bitte versuchen Sie es nochmal !!");
    }
  }

  /**
   * Validate the data to create a new Address
   * @returns True or False
   */
  const validator = (): boolean => {
    return (
      id !== "" &&
      firstname !== "" &&
      lastname !== "" &&
      address !== "" &&
      telefonnumber !== "" &&
      email !== "" &&
      birthsday !== ""
    );
  };
  /**
   * Create
   */
  async function create(): Promise<void> {
    const token = localStorage.getItem("jwt");
    if (validator() == true) {
      let responseData: any = null;
      const request = await fetch(
        "http://localhost:8081/api/v1/adressBuch/create",
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-type": "application/json",
          },
          body: JSON.stringify({
            id: id,
            vorname: firstname,
            nachname: lastname,
            adresse: address,
            telefonNumber: telefonnumber,
            foto: image,
            email: email,
            geburtstag: birthsday,
          }),
        }
      )
        .then((response) => response.json())
        .then((data) => (responseData = data))
        .catch((err) => console.log(err));
      if (responseData.message === "already exists") {
        alert("Diese Id existiert bereit !!");
      } else {
        getAllAddress();
      }
    } else {
      alert("Bitte fühlen Sie alle eingaben ein !!");
    }
  }

  function setSearcher(address: myAddress): void {
    localStorage.setItem("idToSearch", address.id);
    store.setAddress(address);
    navigate("/address");
  }

  return (
    <div className="home">
      {localStorage.getItem("jwt") !== "" ? (
        <div>
          <div className="home-navbar">
            <div>
              <img src={logo} className="logo" />
            </div>
            <div>
              <button className="logout" onClick={() => logout()}>
                Logout
              </button>
            </div>
          </div>
          <hr className="ligne" />
          <div>
            <input
              type="text"
              placeholder="Which ID are you looking for ?"
              className="searchInput"
              onChange={(e) => setIdToken(e.target.value)}
            />
          </div>
          <div className="mytableHeader">
            <table className="table table-dark table-striped myTable">
              <thead>
                <tr>
                  <th scope="col">id</th>
                  <th scope="col">Vorname</th>
                  <th scope="col">Nachname</th>
                  <th scope="col">Addresse</th>
                  <th scope="col">Email</th>
                  <th scope="col">Telefon</th>
                  <th scope="col">Geburtstadum</th>
                  <th scope="col">Edit</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <input
                      type="text"
                      className="form-control createInput"
                      onChange={(e) => setId(e.target.value)}
                    />
                  </td>
                  <td>
                    <input
                      type="text"
                      className="form-control createInput"
                      onChange={(e) => setFirstname(e.target.value)}
                    />
                  </td>
                  <td>
                    <input
                      type="text"
                      className="form-control createInput"
                      onChange={(e) => setLastname(e.target.value)}
                    />
                  </td>
                  <td>
                    <input
                      type="text"
                      className="form-control createInput"
                      onChange={(e) => setaddress(e.target.value)}
                    />
                  </td>
                  <td>
                    <input
                      type="email"
                      className="form-control createInput"
                      onChange={(e) => setMail(e.target.value)}
                    />
                  </td>
                  <td>
                    <input
                      type="text"
                      className="form-control createInput"
                      onChange={(e) => setTelefonnumber(e.target.value)}
                    />
                  </td>
                  <td>
                    <input
                      type="date"
                      className="form-control createInput"
                      onChange={(e) => setBirthsday(e.target.value)}
                    />
                  </td>
                  <td className="editIcons">
                    <BsPlusCircleFill
                      className="editIcon"
                      onClick={() => create()}
                    />
                  </td>
                </tr>
                {addresses
                  ?.filter((address, index) => {
                    if (idToken === "") {
                      return address;
                    } else if (address.id.includes(idToken)) {
                      return address;
                    }
                  })
                  .map((address) => (
                    <tr>
                      <td>{address.id}</td>
                      <td>{address.vorname}</td>
                      <td>{address.nachname}</td>
                      <td>{address.adresse}</td>
                      <td>{address.email}</td>
                      <td>{address.telefonNumber}</td>
                      <td>{address.geburtstag}</td>
                      <td>
                        <div className="editIcons">
                          <BsFillPencilFill
                            className="editIcon"
                            onClick={() => setSearcher(address)}
                          />
                          <BsFillTrashFill
                            className="removeIcon"
                            onClick={() => removeOne(address.id)}
                          />
                        </div>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      ) : (
        <Login />
      )}
    </div>
  );
}

export default observer(Adresses);

import { useState, useEffect, useRef } from "react";
import "./Register.css";
import { BsXCircleFill } from "react-icons/bs";
import { Link, Navigate, useNavigate } from "react-router-dom";

function Register() {
  interface user {
    id: number;
    username: string;
    password: string;
    role: string;
  }
  const navigate = useNavigate();
  const userNameRegex: RegExp = /^[a-zA-Z][a-zA-Z0-9-_]{10,30}/;
  // const passwordRegex: RegExp =  /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!#$%]){10,30}/;

  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [passwordMatcher, setPasswordMatcher] = useState<string>("");
  const [usernameErrorHandler, setusernameErrorHandler] = useState<string>("");
  const [passwordErrorHandler, setpasswordErrorHandler] = useState<string>("");
  const [passwordMatcherErrorHandler, setpasswordMatcherErrorHandler] =
    useState<string>("");

  /**
   * Error Handler for username
   */
  useEffect(() => {
    if (username !== "" && !userNameRegex.test(username)) {
      setusernameErrorHandler("username not valide");
    } else {
      setusernameErrorHandler("");
    }
  }, [username]);

  /**
   * Error Handler for Password
   */
  useEffect(() => {
    if (password === "") {
      setpasswordErrorHandler("Password not valide");
    } else {
      setpasswordErrorHandler("");
    }
  }, [password]);

  /**
   * Error Handler for PasswordMatcher
   */
  useEffect(() => {
    if (passwordMatcher !== password) {
      setpasswordMatcherErrorHandler("Password does not match");
    } else {
      setpasswordMatcherErrorHandler("");
    }
  }, [passwordMatcher, password]);

  const validateUserName = (): boolean => {
    if (userNameRegex.test(username)) {
      return true;
    } else {
      return false;
    }
  };

  const validatePassword = (): boolean => {
    return password === passwordMatcher;
  };

  /**
   * Registrierung
   * @param username
   * @param password
   */
  async function register(username: string, password: string): Promise<void> {
    if (validateUserName() === true && validatePassword() === true) {
      const request = await fetch(
        "http://localhost:8081/api/v1/adressBuch/register",
        {
          method: "POST",
          headers: {
            "Content-type": "application/json",
          },
          body: JSON.stringify({
            id: 20,
            username: username,
            password: password,
            role: "USER",
          }),
        }
      );
      const response = await request.json();
      let userToken: any = null;
      userToken = {
        username: response.username,
        password: response.password,
        id: response.id,
        role: response.role,
      };
      if (userToken === null) {
        alert("Please try it again !");
      } else {
        console.log(userToken);
        navigate("/login");
      }
    } else {
      alert("Bitte die Daten anpassen !");
    }
  }

  return (
    <div className="page">
      <div className="cover">
        <h1 className="title">Registrierung</h1>
        <input
          type="text"
          placeholder="username"
          onChange={(event) => setUsername(event.target.value)}
        />
        {usernameErrorHandler === "" ? (
          <div className="offscreen"></div>
        ) : (
          <div className="err">
            <BsXCircleFill className="icon" />
            {usernameErrorHandler}
          </div>
        )}
        <input
          type="password"
          placeholder="password"
          onChange={(event) => setPassword(event.target.value)}
        />
        <input
          type="password"
          placeholder="Validate password"
          onChange={(event) => setPasswordMatcher(event.target.value)}
        />
        {passwordMatcherErrorHandler === "" ? (
          <div className="offscreen"></div>
        ) : (
          <div className="err">
            <BsXCircleFill className="icon" />
            {passwordMatcherErrorHandler}
          </div>
        )}
        <button className="btn" onClick={() => register(username, password)}>
          Registierung
        </button>
        <Link className="btn" to="/">
          Login
        </Link>
      </div>
    </div>
  );
}

export default Register;

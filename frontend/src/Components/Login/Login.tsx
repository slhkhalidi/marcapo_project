import { useEffect, useState } from "react";
import "./Login.css";
import { Link, useNavigate } from "react-router-dom";
import { observer } from "mobx-react";
import store from "../MobX-Store/store";

function Login() {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const navigate = useNavigate();

  const dataCheker = (): boolean => {
    return username !== "" && password !== "";
  };

  /**
   * Login
   */
  const login = async (): Promise<void> => {
    if (dataCheker() === true) {
      const request = await fetch(
        `http://localhost:8081/login?username=${username}&password=${password}`,
        {
          method: "POST",
          headers: {
            "Content-type": "application/json",
          },
        }
      );
      const response = await request.json();
      if (response.status === "error") {
        alert("username or password are not correct !");
      } else {
        const myToken: string = response.jwtToken;
        localStorage.setItem("jwt", myToken);
        store.setJwt(myToken);
        console.log(myToken);
        navigate("/home");
      }
    } else {
      // TODO
      alert("Data are not correct");
    }
  };

  return (
    <div className="page">
      <div className="cover">
        <h1 className="title">Login</h1>
        <input
          type="text"
          placeholder="username"
          onChange={(e) => setUsername(e.target.value)}
        />
        <input
          type="password"
          placeholder="password"
          onChange={(e) => setPassword(e.target.value)}
        />
        <button className="btn" onClick={() => login()}>
          Login
        </button>
        <Link className="btn" to="/registrierung">
          Create Account
        </Link>
      </div>
    </div>
  );
}

export default observer(Login);

package com.example.demo.error;

import java.util.Date;

public class ErrorDetails {

    private String message;
    private Long timeStamps;

    public ErrorDetails(String message,  Long timeStamps) {
        this.message = message;
        this.timeStamps = timeStamps;
    }


    public String getMessage() {
        return message;
    }


    public Long getTimeStamps() {
        return timeStamps;
    }


    public void setMessage(String message) {
        this.message = message;
    }

    public void setTimeStamps(Long timeStamps) {
        this.timeStamps = timeStamps;
    }
}

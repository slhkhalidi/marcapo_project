package com.example.demo.adresse_api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/adressBuch")
public class AdresseController {

    @Autowired
    private AdresseService adresseService;
    @Autowired
    private AdresseRepository adresseRepository;

    @GetMapping
    public ResponseEntity<String> helloWorld() {
        return new ResponseEntity<>("Welcome to AddressBuch API", HttpStatus.OK);
    }


    /**
     * Get All
     * @return List of Address
     */
    @GetMapping("/all")
     public ResponseEntity<List<Adresse>> getAllAddresses() {
        return adresseService.getAll();
     }


    /**
     * Get Address By ID
     * @param id
     * @return
     */
     @GetMapping("/{id}")
    public Adresse getAddressById(@PathVariable String id) {
        return adresseRepository.findById(id).get();
     }



    /**
     * Create a New Address
     * @param adresse
     */
    @PostMapping("/create")
    public ResponseEntity<Adresse> createAddress(@RequestBody Adresse adresse) {
       return adresseService.createAddress(adresse);
   }




    /**
     * Update Address
     * @param id
     * @param address
     * @return
     */
    @PutMapping("/update/{id}")
    public Map<String,String> updateAddress(@PathVariable String id, @RequestBody Adresse address) {
        return adresseService.updateAddress(id, address);
    }




    /**
     * Delete Address By ID
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteById (@PathVariable  String id) {
        return adresseService.deleteAddressById(id);
    }

}

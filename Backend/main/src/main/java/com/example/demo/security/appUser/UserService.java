package com.example.demo.security.appUser;

import com.example.demo.error.AlreadyExistsException;
import com.example.demo.error.NotFoundException;
import com.example.demo.security.appUser.AppUser;
import com.example.demo.security.appUser.AppUserRepository;
import com.example.demo.security.jwtToken.Token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.NoSuchElementException;


@Service
public class UserService implements UserDetailsService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            AppUser user  = appUserRepository.findAppUserByUsername(username).get();
            Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority("USER");
            authorities.add(authority);
            return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
        } catch (NoSuchElementException ex) {
            throw new NotFoundException("User not found");
        }
    }








    /**
     * Register the User in DB
     * @param user
     * @return
     */
    public ResponseEntity<AppUser> addUser (AppUser user ) {
        boolean userChecker = appUserRepository.findAppUserByUsername(user.getUsername()).isPresent();
        if(userChecker == false) {
            String passwordEncoded = passwordEncoder.encode(user.getPassword());
            user.setPassword(passwordEncoded);
            appUserRepository.save(user);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } else {
            throw new AlreadyExistsException("UserName is already exists");
        }
    }



/**
    public  ResponseEntity<AppUser> loginUser(String username, String password) {
        try {
            AppUser myUsertoken = appUserRepository.findAppUserByUsernameAndPassword(username, password).get();
            String myJWT = Token.generateToken(username,password);
            HttpHeaders responseHeader = new HttpHeaders();
            responseHeader.set("acces_token", myJWT);
            return new ResponseEntity<>(myUsertoken,responseHeader , HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            throw new NotFoundException("User not found");
        }
    }  **/



}

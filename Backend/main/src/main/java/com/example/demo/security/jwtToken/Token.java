package com.example.demo.security.jwtToken;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class Token {



    public static String generateToken(String username, String password) {
        // This is the VERIFY SIGNATURE, The method getBytes() encodes a String into a byte array //
        // Claims is like attribute in the context of JWT //
        Algorithm algorithm = Algorithm.HMAC256("mySecretKey".getBytes());
        String token = JWT.create()
                .withSubject("test")
                .withExpiresAt(new Date(System.currentTimeMillis() + 30 * 60 * 1000))
                .withIssuer("Tester")
                .withClaim("username", username)
                .withClaim("password", password)
                .sign(algorithm);
        return token;
    }




}

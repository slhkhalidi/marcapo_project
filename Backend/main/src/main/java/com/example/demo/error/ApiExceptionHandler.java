package com.example.demo.error;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import java.util.Date;


@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorDetails> handleNotFoundException(NotFoundException ex) {
        Date date = new Date();
        long timestamps =  date.getTime();
        ErrorDetails error = new ErrorDetails(ex.getMessage(),timestamps);
        return new ResponseEntity<>(error,ex.getStatusCode());
    }


    @ExceptionHandler(AlreadyExistsException.class)
    public ResponseEntity<ErrorDetails>  AlreadyExistsExceptionHandler ( AlreadyExistsException ex) {
        Date date = new Date();
        long timestamps =  date.getTime();
        ErrorDetails error = new ErrorDetails(ex.getMessage(), timestamps);
        return new ResponseEntity<>(error, ex.getStatusCode());
    }



}

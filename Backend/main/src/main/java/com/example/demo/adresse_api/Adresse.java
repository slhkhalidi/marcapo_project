package com.example.demo.adresse_api;

import com.mongodb.lang.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Adressen")
public class Adresse {

    @Id
    @Indexed(unique = true)
    private String id;
    private String vorname;
    private String nachname;
    private String adresse;
    private String telefonNumber;
    private String foto;
    @Indexed(unique = true)
    private String email;
    private String geburtstag;



    public Adresse() {
    }

    public Adresse(String vorname, String nachname, String adresse, String telefonNumber, String foto, String email, String geburtstag,String id) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.adresse = adresse;
        this.telefonNumber = telefonNumber;
        this.foto = foto;
        this.email = email;
        this.geburtstag = geburtstag;
        this.id = id;
    }


    /**
     * Getters
     */
    public String getId() {
        return id;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getTelefonNumber() {
        return telefonNumber;
    }

    public String getFoto() {
        return foto;
    }

    public String getEmail() {
        return email;
    }

    public String getGeburtstag() {
        return geburtstag;
    }


    /**
     * Setters
     */
    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setTelefonNumber(String telefonNumber) {
        this.telefonNumber = telefonNumber;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGeburtstag(String geburtstag) {
        this.geburtstag = geburtstag;
    }

    public void setId(String id) {
        this.id = id;
    }
}

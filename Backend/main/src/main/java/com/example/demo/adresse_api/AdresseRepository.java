package com.example.demo.adresse_api;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * With Repository Annotations we say to the Spring that this class is Bean and we can inject it into another class
 */
@Repository
public interface AdresseRepository extends MongoRepository<Adresse, String> {


}

package com.example.demo.security.appUser;

import com.example.demo.security.appUser.AppUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppUserRepository extends MongoRepository<AppUser, Integer> {

    Optional<AppUser> findAppUserByUsernameAndPassword(String userName, String password);


    Optional<AppUser> findAppUserByUsername(String userName);

}

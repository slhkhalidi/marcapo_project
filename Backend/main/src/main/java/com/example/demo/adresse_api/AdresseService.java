package com.example.demo.adresse_api;


import com.example.demo.error.AlreadyExistsException;
import com.example.demo.error.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@Service
public class AdresseService {

    /**
     * With  Autowired we make a Dependency Injection of the Object form the class AdresseRepository
     * We must not declare the object with = new AdresseRepository;
     * Damit wir unabängig mit der Class arbeiten
     */
    @Autowired
    private AdresseRepository adresseRepository;


    /**
     * Get All Addresses
     *
     * @return
     */
    public ResponseEntity<List<Adresse>> getAll() {
        List<Adresse> addressList = adresseRepository.findAll();
        return new ResponseEntity<>(addressList,HttpStatus.OK);
    }


    /**
     * getAddressById
     * @param id
     * @return an Address or Exception
     */
    public Adresse getAddressById(HttpServletRequest request, HttpServletResponse response, String id) {
        try {
            Adresse token = adresseRepository.findById(id).get();
            return token;
        }catch(NoSuchElementException ex) {
            throw new NotFoundException("No matched Address with this Id: "+id);
        }
    }


    /**
     * Create a new Address and save it into DB
     *
     * @param adresse
     */
    public ResponseEntity<Adresse> createAddress(Adresse adresse) {
        try {
            adresseRepository.insert(adresse);
            return  new ResponseEntity<>(adresse, HttpStatus.OK);
        } catch(Exception e) {
            throw new AlreadyExistsException("already exists");
        }
    }






    /**
     * Update Address
     * @param id
     * @param address
     */
    public Map<String, String> updateAddress(String id, Adresse address) {
         Adresse token = null;
         token = adresseRepository.findById(id).get();
         if(token==null) {
             throw new NotFoundException("This Address does not exists");
         } else {
             adresseRepository.save(address);
             Map<String,String> response =new HashMap<>();
             response.put("status","success");
             return response;
         }
    }






    /**
     * Delete Address
     * @param id
     */
    public ResponseEntity<String> deleteAddressById(String id) {
        Adresse token = null;
        token = adresseRepository.findById(id).get();
        if(token==null) {
            throw new NotFoundException("This Address Id does not exists");
        } else {
            adresseRepository.delete(token);
            return new ResponseEntity<>("OK",HttpStatus.OK);
        }
    }



}


package com.example.demo.security.appUser;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/adressBuch")
public class UserController {

    @Autowired
    private UserService userService;


    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/register")
    public ResponseEntity<AppUser> addUser(@RequestBody AppUser user) {
        return userService.addUser(user);
    }




}
